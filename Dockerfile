FROM golang:alpine AS builder
RUN wget -O - https://raw.githubusercontent.com/golang/dep/master/install.sh | sh

ADD . /go/src/prometheus-domain-expiration-exporter
WORKDIR /go/src/prometheus-domain-expiration-exporter
RUN go mod init prometheus-domain-expiration-exporter
RUN dep ensure 
RUN go build -mod=mod -o prometheus-domain-expiration-exporter .

FROM alpine
COPY --from=builder /go/src/prometheus-domain-expiration-exporter/prometheus-domain-expiration-exporter /usr/bin/prometheus-domain-expiration-exporter
CMD ["/usr/bin/prometheus-domain-expiration-exporter"]
